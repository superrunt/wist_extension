var canvas = new fabric.Canvas('c');
var fitButton = document.getElementById("fitButton")
var cropButton = document.getElementById("cropButton")
var saveButton = document.getElementById("saveButton")
var uploadButton = document.getElementById("uploadButton")
var cancelUploadButton = document.getElementById("cancelUploadButton")
var pointButton = document.getElementById("pointButton")
var regionButton = document.getElementById("regionButton")
var deleteButton = document.getElementById("deleteButton")
var jsonDiv = document.getElementById("json")
var thingName = document.getElementById("thingName")
var templateName = document.getElementById("templateName")
var ssImg
var pixelRatio
var json = {}
var selectedThing
var stuffIndex = 0

saveButton.addEventListener("click", function () {
    const dataUrl = ssImg.toDataURL({ "format": "png" })
    chrome.runtime.sendMessage({ "cmd": "save", "data": dataUrl, "name": templateName.value, "json": JSON.stringify(json) }, function (response) {
        console.log(`message from background: ${JSON.stringify(response)}`);
    });
})

uploadButton.addEventListener("click", function () {
    $('div#uploadInput').show()
    $('button#uploadButton').hide()
})
cancelUploadButton.addEventListener("click", function () {
    $('div#uploadInput').hide()
    $('button#uploadButton').show()
})

deleteButton.addEventListener("click", function () {
    if (selectedThing) {
        canvas.remove(selectedThing)
        selectedThing = null
        thingName.value = ""
        updateJson()
    }
})

thingName.addEventListener("change", function (e) {
    if (selectedThing) {
        type = selectedThing.type == 'point' ? 'points' : 'regions'
        thingInJson = json[type][selectedThing.name]
        delete json[type][selectedThing.name]
        console.log(json)
        selectedThing.name = thingName.value
    }
    updateJson()
})

canvas.on("selection:created", function (e) {
    if (Object.keys(e.target).indexOf("name") != -1) {
        thingName.value = e.target.name
        selectedThing = e.target
    }
})

canvas.on("selection:cleared", function (e) {
    if (selectedThing) {
        selectedThing.name = thingName.value
        thingName.value = ""
        selectedThing = null
    }
})

pointButton.addEventListener("click", function () {
    circle = new fabric.Circle({
        radius: 5,
        fill: "green",
        lockScalingX: true,
        lockScalingY: true,
        hasControls: false
    })

    addThingy(circle)
})

regionButton.addEventListener("click", function () {
    rect = new fabric.Rect({
        width: 100,
        height: 100,
        lockRotation: true,
        stroke: "blue",
        fill: "transparent"
    })

    rect.on('scaled', function (e) {
        const w = e.target.width * e.target.scaleX
        const h = e.target.height * e.target.scaleY

        e.target.set({
            "height": h,
            "width": w,
            "scaleX": 1,
            "scaleY": 1
        })
    })

    addThingy(rect)
})

function addThingy(element, shouldUpdateJSON = true) {
    canvas.add(element)
    element.viewportCenter()
    element.name = element.name ? element.name : element.type + "_" + stuffIndex
    stuffIndex++
    if (shouldUpdateJSON) updateJson()

    element.on("moved", function (e) {
        updateJson()
    })

    element.on("selected", function (e) {
        selectedThing = element
        thingName.value = element.name
    })
}

function updateJson() {
    canvas.forEachObject(function (s) {
        if (Object.keys(s).indexOf("name") != -1) {
            if (s.type == "circle") {
                if (Object.keys(json).indexOf("points") == -1) {
                    json["points"] = {}
                }
                json.points[s.name] = {
                    position: [Math.round(s.left), Math.round(s.top)],
                    type: 'circle'
                }
            } else if (s.type == "rect") {
                if (Object.keys(json).indexOf("regions") == -1) {
                    json["regions"] = {}
                }

                json.regions[s.name] = { 
                    top_left: [Math.round(s.left), Math.round(s.top)], 
                    bottom_right: [Math.round(s.left + s.width), Math.round(s.top + s.height)],
                    type: 'rect'
                }
            }
        }
    })

    json.canvasData = canvas
    setJSONText()
}

function setJSONText () {
    const regions = json.regions ? JSON.stringify(json.regions) : 'No regions have been created.'
    const points = json.points ? JSON.stringify(json.points) : 'No points have been created.'
    jsonDiv.innerText = `Regions: ${regions}\r\rPoints: ${points}`
}

var cropRect
cropButton.addEventListener("click", function () {
    if (!cropRect) {
        cropRect = new fabric.Rect({
            left: 100,
            top: 100,
            width: 100,
            height: 100,
            lockRotation: true,
            fill: 'transparent',
            stroke: 'red',
            strokeWidth: 5
        })

        cropRect.on('scaled', function (e) {
            console.log('scaled', e)
            const w = cropRect.width * cropRect.scaleX
            const h = cropRect.height * cropRect.scaleY

            cropRect.set({
                "height": h,
                "width": w,
                "scaleX": 1,
                "scaleY": 1
            })
        })
        canvas.add(cropRect)

        cropButton.innerHTML = '<span class="oi oi-check" title="check" aria-hidden="true"></span>'
    } else {
        const dataUrl = ssImg.toDataURL({
            "format": "png",
            "left": cropRect.left,
            "top": cropRect.top,
            "width": cropRect.width,
            "height": cropRect.height
        })
        fabric.Image.fromURL(dataUrl, loadedImage)
        canvas.remove(cropRect)
        cropRect = null
        cropButton.innerHTML = '<span class="oi oi-crop" title="crop" aria-hidden="true"></span>'
        resetView()
    }
})

canvas.on("mouse:wheel", function (opt) {
    var delta = opt.e.deltaY
    var zoom = canvas.getZoom()
    zoom = zoom - delta / 200;
    if (zoom > 2) zoom = 2;
    if (zoom < 0.1) zoom = 0.1;
    canvas.setZoom(zoom);
    opt.e.preventDefault();
    opt.e.stopPropagation();
})

canvas.on('mouse:down', function (opt) {
    var evt = opt.e
    if (evt.altKey === true) {
        this.isDragging = true;
        this.selection = false;
        this.lastPosX = evt.clientX;
        this.lastPosY = evt.clientY;
    }
});

canvas.on('mouse:move', function (opt) {
    if (this.isDragging) {
        var e = opt.e;
        this.viewportTransform[4] += e.clientX - this.lastPosX;
        this.viewportTransform[5] += e.clientY - this.lastPosY;
        this.requestRenderAll();
        this.lastPosX = e.clientX;
        this.lastPosY = e.clientY;
    }
});

canvas.on('mouse:up', function (opt) {
    this.isDragging = false;
    this.selection = true;
});

fitButton.addEventListener("click", resetView)

function resetView() {
    canvas.absolutePan(new fabric.Point(0, 0))
    let zoomLevel = 1
    if (canvas.width / canvas.height > ssImg.width / ssImg.height) {
        zoomLevel = canvas.height / ssImg.height
    } else {
        zoomLevel = canvas.width / ssImg.width
    }
    canvas.setZoom(zoomLevel)
}

function loadedImage(img, w) {
    if (ssImg) {
        canvas.remove(ssImg)
    }

    ssImg = img
    img.set({
        selectable: false,
        evented: false,
        lockMovementX: true,
        lockMovementY: true,
        lockRotation: true,
        lockScalingX: true,
        lockScalingY: true,
        hasBorders: false,
        hasControls: false,
        lockUniScaling: true
    })
    canvas.add(img)
    resetView()
    if (w) {
        pixelRatio = img.getOriginalSize().width / w
    }
    updateJson()
}

chrome.tabs.captureVisibleTab(null, { "format": "png" }, function (dataUrl) {
    console.log("Setting image...")
    chrome.tabs.executeScript(null, {
        code: "[document.documentElement.clientWidth, document.documentElement.clientHeight]"
    }, function (resp) {
        const clientW = resp[0][0]
        const clientH = resp[0][1]
        fabric.Image.fromURL(dataUrl, function(i) {loadedImage(i, clientW)})
    })
})

function loadCanvasFromJSON (preloadJson) {
    if ( !preloadJson ) {
        alert('there is no data in the file you uploaded.')
        return
    }
    json = {}
    canvas.clear()
    canvas.loadFromJSON(preloadJson.canvasData, canvas.renderAll.bind(canvas), (o, object) => { onObjectLoad(o, object) })
    
}

function onObjectLoad(o, object) {
    stuffIndex++
    object.name = o.name
    // add event listeners to all objects just added
    if (object.type == "rect") {
        object.on('scaled', function (e) {
            const w = e.target.width * e.target.scaleX
            const h = e.target.height * e.target.scaleY
            e.target.set({
                "height": h,
                "width": w,
                "scaleX": 1,
                "scaleY": 1
            })
            updateJson()
        })
    }

    if (object.type == "circle") {
        object.lockScalingX = true
        object.lockScalingY = true
    }

    if (object.type == "image") {
        object.selectable = false
    }

    object.on("moved", function (e) {
        updateJson()
    })

    object.on("added", function (e) {
        console.log("ADDED")
        updateJson()
    })

}

handleFileUpload = (event) => {
    var file = event.target.files[0]
    this.reader.readAsText(file) // fires onload when done.
}

handleFileRead = (event)  => {
    var save = JSON.parse(event.target.result);
    loadCanvasFromJSON(save)
}

var reader = new FileReader()
reader.onload = handleFileRead
document.querySelector('#fileUpload').addEventListener('change', handleFileUpload, false);