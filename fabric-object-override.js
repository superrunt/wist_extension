// override fabric library here so name value can be added to JSON object on JSON.stringify
fabric.Object.prototype.toObject = function(propertiesToInclude) {
    var NUM_FRACTION_DIGITS = fabric.Object.NUM_FRACTION_DIGITS,

        object = {
          type:                     this.type,
          version:                  fabric.version,
          originX:                  this.originX,
          originY:                  this.originY,
          left:                     fabric.util.toFixed(this.left, NUM_FRACTION_DIGITS),
          top:                      fabric.util.toFixed(this.top, NUM_FRACTION_DIGITS),
          width:                    fabric.util.toFixed(this.width, NUM_FRACTION_DIGITS),
          height:                   fabric.util.toFixed(this.height, NUM_FRACTION_DIGITS),
          fill:                     (this.fill && this.fill.toObject) ? this.fill.toObject() : this.fill,
          stroke:                   (this.stroke && this.stroke.toObject) ? this.stroke.toObject() : this.stroke,
          strokeWidth:              fabric.util.toFixed(this.strokeWidth, NUM_FRACTION_DIGITS),
          strokeDashArray:          this.strokeDashArray ? this.strokeDashArray.concat() : this.strokeDashArray,
          strokeLineCap:            this.strokeLineCap,
          strokeLineJoin:           this.strokeLineJoin,
          strokeMiterLimit:         fabric.util.toFixed(this.strokeMiterLimit, NUM_FRACTION_DIGITS),
          scaleX:                   fabric.util.toFixed(this.scaleX, NUM_FRACTION_DIGITS),
          scaleY:                   fabric.util.toFixed(this.scaleY, NUM_FRACTION_DIGITS),
          angle:                    fabric.util.toFixed(this.angle, NUM_FRACTION_DIGITS),
          flipX:                    this.flipX,
          flipY:                    this.flipY,
          opacity:                  fabric.util.toFixed(this.opacity, NUM_FRACTION_DIGITS),
          shadow:                   (this.shadow && this.shadow.toObject) ? this.shadow.toObject() : this.shadow,
          visible:                  this.visible,
          clipTo:                   this.clipTo && String(this.clipTo),
          backgroundColor:          this.backgroundColor,
          fillRule:                 this.fillRule,
          paintFirst:               this.paintFirst,
          globalCompositeOperation: this.globalCompositeOperation,
          transformMatrix:          this.transformMatrix ? this.transformMatrix.concat() : null,
          skewX:                    fabric.util.toFixed(this.skewX, NUM_FRACTION_DIGITS),
          skewY:                    fabric.util.toFixed(this.skewY, NUM_FRACTION_DIGITS),
          name:                     this.name
        };

    if (this.clipPath) {
      object.clipPath = this.clipPath.toObject(propertiesToInclude);
      object.clipPath.inverted = this.clipPath.inverted;
      object.clipPath.absolutePositioned = this.clipPath.absolutePositioned;
    }

    fabric.util.populateWithProperties(this, object, propertiesToInclude);
    if (!this.includeDefaultValues) {
      object = this._removeDefaultValues(object);
    }

    return object;
  }

  fabric.Object.prototype.stateProperties = (
    'top left width height scaleX scaleY flipX flipY originX originY transformMatrix ' +
    'stroke strokeWidth strokeDashArray strokeLineCap strokeLineJoin strokeMiterLimit ' +
    'angle opacity fill globalCompositeOperation shadow clipTo visible backgroundColor ' +
    'skewX skewY fillRule paintFirst name'
  ).split(' ')